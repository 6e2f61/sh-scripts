#!/bin/bash
# source of original solution: https://stackoverflow.com/a/31546962
echo "Enter variable name: "
read variable_name
echo "Enter variable value: "
read variable_value
echo "adding " $variable_name " to environment variables: " $variable_value
echo "export "$variable_name"="$variable_value>>~/.bashrc
echo $variable_name"="$variable_value>>~/.bash_profile
echo $variable_name"="$variable_value>>/etc/environment
source ~/.bashrc
source ~/.bash_profile
echo "Do you want to restart your computer to apply changes in /etc/environment file? yes(y) no(N)"
read restart
case $restart in
	y) shutdown -r;;
	*) echo "Don't forget to restart your computer manually"
esac
